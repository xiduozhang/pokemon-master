# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'

csv_text = File.read(Rails.root.join('lib', 'seeds', 'pokemon.csv'))

csv = CSV.parse(csv_text, headers: true, encoding: 'utf-8')
count = 0
Pokemon.transaction do
    csv.each do |row|
        Pokemon.create!(
            poke_index: row['#'].to_i,
            name: row['Name'],
            type_1: row['Type 1'],
            type_2: row['Type 2'],
            total: row['Total'].to_i,
            hp: row['HP'].to_i,
            attack: row['Attack'].to_i,
            defense: row['Defense'].to_i,
            speed_attack: row['Sp. Atk'].to_i,
            speed_defense: row['Sp. Def'].to_i,
            speed: row['Speed'].to_i,
            generation: row['Generation'].to_i,
            legendary: row['Legendary'].downcase == 'false' ? false : true
        )
        count = count + 1
    end
end

puts "#{count} pokemons saved!"
# should save 800 pokemons
