# **GET** #
## -Get paging list pokemons ##

**URL**: `/api/pokemons`

**Method**: `GET`

## Success Response ##

**Code**: `200 OK`

**Content examples**

A list of pokemons present in a table

## -Get all pokemons in json ##

**URL**: `/api/pokemons.json`

**Method**: `GET`

## Succes Response ##

**Code**: `200 OK`

**Content examples**

A array of pokemons in json

## -Get a pokemon by name ##

**URL**: `/api/pokemons/?name='name'`

**Method**: `GET`

## Succes Response ##

**Code**: `200 OK`

**Content examples**

Get a pokemon with a name of 'klink'
```json
{
	"id": 6278,
	"poke_index": 599,
	"name": "Klink",
	"type_1": "Steel",
	"type_2": null,
	"total": 300,
	"hp": 40,
	"attack": 55,
	"defense": 70,
	"speed_attack": 45,
	"speed_defense": 60,
	"speed": 30,
	"generation": 5,
	"legendaray": false
	
}
```


# **Create** #

Create a new Pokemon

**URL**: `/api/pokemons/?name='name'*poke_index='poke_index'`

**Method**: `POST`

**Data constraints**

```json
{
	"poke_index": "can not be null",
	"name": "can not be null and unique",
    "type_1": "can be null",
    "type_2": "can be null",
    "total": "Total of hp + attack + defense + spd. atk. + spd.def. + speed",
    "hp": "by default 1",
    "attack": "by default 1",
    "defense": "by default 1",
    "speed_attack": "by default 1",
    "speed_defense": "by default 1",
    "speed": "by default 1",
    "generation": "by default 1",
    "legendary": "by default 1",
}
```

## Succes Response ##

**Code**: `200 OK`

**Content examples**

Create a pokemon with a name of 'Sword' and infomations about this new pokemon
```json
{
	"id": 6278,
	"poke_index": 599,
	"name": "Sword"
	"type_1": "Steel",
	"type_2": null,
	"total": 300,
	"hp": 40,
	"attack": 55,
	"defense": 70,
	"speed_attack": 45,
	"speed_defense": 60,
	"speed": 30,
	"generation": 5,
	"legendaray": false
	
}
```
## Error Response ##

**Condition **: If name already existed.

**Code**: `422 unprocessable entity`

**Content example**

```json
	{
		"name": [
			"has already been taken"
		]
	}
```

# **Update** #

Update a new Pokemon

**URL**: `/api/pokemons/'id'`

**Method**: `PUT`

**Data constraints**

Same as a creation

## Succes Response ##

**Code**: `200 OK`

**Content examples**

Same as creation

## Error Response ##

**Condition **: If name already existed.

**Code**: `422 unprocessable entity`

**Content example**

```json
	{
		"name": [
			"has already been taken"
		]
	}
```

# **DELETE** #

Create a new Pokemon

**URL**: `/api/pokemons/'id'`

**Method**: `DELETE`

## Succes Response ##

**Code**: `200 OK`
