json.array! @pokemons do |p|
    json.poke_index p.poke_index
    json.name p.name
    json.type_1 p.type_1
    json.type_2 p.type_2
    json.total p.total
    json.hp p.hp
    json.attack p.attack
    json.defense p.defense
    json.speed_attack p.speed_attack
    json.speed_defense p.speed_defense
    json.speed p.speed
    json.generation p.generation
    json.legendary p.legendary
end